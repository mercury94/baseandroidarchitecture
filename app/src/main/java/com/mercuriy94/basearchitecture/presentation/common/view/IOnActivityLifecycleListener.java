package com.mercuriy94.basearchitecture.presentation.common.view;

/**
 * Created by Nikita on 09.04.2017.
 */

public interface IOnActivityLifecycleListener {

    void onStart();

    void onStop();

}
