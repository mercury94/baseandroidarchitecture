package com.mercuriy94.basearchitecture.presentation.module.main.presenter.assembly;

import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.PresenterModule;
import com.mercuriy94.basearchitecture.presentation.di.scope.PresenterScope;
import com.mercuriy94.basearchitecture.presentation.module.main.presenter.MainPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;


/**
 * Created by Nikita on 05.05.2017.
 */
@Module
public class MainPresenterModule extends PresenterModule<MainPresenter> {

    protected MainPresenterModule(MainPresenter mainPresenter) {
        super(mainPresenter);
    }


}
