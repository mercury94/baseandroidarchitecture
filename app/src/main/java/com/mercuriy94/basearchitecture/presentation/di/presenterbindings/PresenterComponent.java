package com.mercuriy94.basearchitecture.presentation.di.presenterbindings;

import com.mercuriy94.basearchitecture.presentation.common.presenter.BasePresenter;

import dagger.MembersInjector;

/**
 * Created by Nikita on 05.05.2017.
 */

public interface PresenterComponent<Presenter extends BasePresenter> extends MembersInjector<Presenter>{
}
