package com.mercuriy94.basearchitecture.presentation.di.objectbox;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.data.entity.CurrentUser;
import com.mercuriy94.basearchitecture.data.entity.MyObjectBox;
import com.mercuriy94.basearchitecture.presentation.di.app.AppModule;

import dagger.Module;
import dagger.Provides;
import io.objectbox.Box;
import io.objectbox.BoxStore;


/**
 * Created by nikita on 25.12.2016.
 */
@Module(includes = AppModule.class)
public class ObjectBoxModule {

    private final static String MY_OBJECT_BOX = "myhomeobjectbox";
    public final static String QUALIFER_MY_HOME_REALM_CONFIG = "my_home_config";


    @NonNull
    @Provides
    public BoxStore provideObjectBox(Context context) {
        return MyObjectBox.builder()
                .androidContext(context)
                .name(MY_OBJECT_BOX)
                .build();
    }


    @NonNull
    @Provides
    public Box<CurrentUser> provideBoxCurrentUser(BoxStore boxStore) {
        return boxStore.boxFor(CurrentUser.class);
    }


}
