package com.mercuriy94.basearchitecture.presentation.common.router;

import android.content.Intent;

/**
 * Created by Nikita on 10.04.2017.
 */

public interface IBaseRouteConductor {

    void navigateTo(Intent intent);

}
