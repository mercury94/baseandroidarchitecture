package com.mercuriy94.basearchitecture.presentation.module.main.presenter;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.basearchitecture.presentation.model.TitleModel;
import com.mercuriy94.basearchitecture.presentation.module.main.MainScreenContract;

import java.net.HttpURLConnection;

import javax.inject.Inject;




/**
 * Created by Nikita on 06.04.2017.
 */

@InjectViewState
public class MainPresenter extends MainScreenContract.AbstractMainPresenter {

    private static final String TAG = "MainPresenter";



    public MainPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super(presenterSubcomponentBuilders);
    }

    @Override
    public void inject(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super.inject(presenterSubcomponentBuilders);

    }

    @Override
    protected TitleModel getTitle() {
        return null;
    }
}
