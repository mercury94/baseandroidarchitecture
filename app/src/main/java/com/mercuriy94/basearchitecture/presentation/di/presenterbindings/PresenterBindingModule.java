package com.mercuriy94.basearchitecture.presentation.di.presenterbindings;

import com.mercuriy94.basearchitecture.presentation.module.main.presenter.assembly.IMainPresenterSubcomponent;
import com.mercuriy94.basearchitecture.presentation.module.main.presenter.MainPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


/**
 * Created by Nikita on 05.05.2017.
 */

@Module(subcomponents = {
        IMainPresenterSubcomponent.class})
public abstract class PresenterBindingModule {


    @Binds
    @IntoMap
    @PresenterKey(MainPresenter.class)
    public abstract PresenterComponentBuilder bindMainPresenterComponent(IMainPresenterSubcomponent.Builder impl);

}
