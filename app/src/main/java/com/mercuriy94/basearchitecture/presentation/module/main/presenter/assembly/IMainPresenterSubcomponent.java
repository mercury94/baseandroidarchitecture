package com.mercuriy94.basearchitecture.presentation.module.main.presenter.assembly;

import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.PresenterComponent;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.PresenterComponentBuilder;
import com.mercuriy94.basearchitecture.presentation.di.scope.PresenterScope;
import com.mercuriy94.basearchitecture.presentation.module.main.presenter.MainPresenter;

import dagger.Subcomponent;


/**
 * Created by Nikita on 18.04.2017.
 */
@PresenterScope
@Subcomponent(modules = MainPresenterModule.class)
public interface IMainPresenterSubcomponent extends PresenterComponent<MainPresenter> {

    @Subcomponent.Builder
    interface Builder extends PresenterComponentBuilder<MainPresenterModule, IMainPresenterSubcomponent> {

    }

}
