package com.mercuriy94.basearchitecture.presentation.di.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.data.repository.currentuser.local.CurrentUserLocalRepository;
import com.mercuriy94.basearchitecture.data.repository.currentuser.local.ICurrentUserLocalRepository;
import com.mercuriy94.basearchitecture.data.repository.settings.connection.local.ConnectionSettingsLocalRepository;
import com.mercuriy94.basearchitecture.data.repository.settings.connection.local.IConnectionSettingsLocalRepository;
import com.mercuriy94.basearchitecture.presentation.di.objectbox.ObjectBoxModule;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.objectbox.Box;



/**
 * Created by nikita on 27.12.2016.
 */

@Module(includes = {ObjectBoxModule.class, LocalRepositoryModule.Declarations.class,})
public class LocalRepositoryModule {

    @NonNull
    @Provides
    public SharedPreferences provideSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }



    @Module
    public abstract class Declarations {

        @NonNull
        @Binds
        public abstract IConnectionSettingsLocalRepository bindConnectionSettingsLocalRepository(ConnectionSettingsLocalRepository connectionSettingsLocalRepository);

        @NonNull
        @Binds
        public abstract ICurrentUserLocalRepository bindCurrentUserLocalRepository(CurrentUserLocalRepository currentUserLocalRepository);

    }

}
