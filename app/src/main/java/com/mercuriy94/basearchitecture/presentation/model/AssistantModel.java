package com.mercuriy94.basearchitecture.presentation.model;

/**
 * Created by Nikita on 14.05.2017.
 */

public class AssistantModel {

    protected String mUuid;

    protected String mName;

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
