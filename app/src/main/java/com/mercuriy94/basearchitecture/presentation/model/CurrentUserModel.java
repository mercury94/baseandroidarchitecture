package com.mercuriy94.basearchitecture.presentation.model;

/**
 * Created by Nikita on 08.05.2017.
 */

public class CurrentUserModel {

    private String mUuid;
    private String mName;
    private String mSurname;
    private String mAvatarImage;

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getAvatarImage() {
        return mAvatarImage;
    }

    public void setAvatarImage(String avatarImage) {
        mAvatarImage = avatarImage;
    }
}
