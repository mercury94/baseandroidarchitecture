package com.mercuriy94.basearchitecture.presentation.common.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpPresenter;
import com.mercuriy94.basearchitecture.presentation.common.router.ActivityRouterAdapter;
import com.mercuriy94.basearchitecture.presentation.common.router.BaseActivityRouter;
import com.mercuriy94.basearchitecture.presentation.common.view.IBaseView;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.basearchitecture.presentation.helper.StringUtils;
import com.mercuriy94.basearchitecture.presentation.model.TitleModel;


/**
 * Created by nikita on 21.01.2017.
 */

public abstract class BasePresenter<View extends IBaseView, Router extends BaseActivityRouter>
        extends MvpPresenter<View> {

    public static final String TAG = "BasePresenter";

    private Router mRouter;


    public BasePresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        inject(presenterSubcomponentBuilders);
    }

    public void inject(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {

    }

    public void setRouter(@NonNull Router router) {
        mRouter = router;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        updateTitle();
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);
        view.setInitialState();

    }

    @SuppressWarnings("unchecked")
    public void setRouteConductor(@NonNull ActivityRouterAdapter routeConductor) {
        if (getRouter() != null) getRouter().setRouterAdapter(routeConductor);
    }


    public Router getRouter() {
        return mRouter;
    }

    public boolean onBackPressed() {
        getRouter().onBackPressed();
        return true;
    }

    protected abstract TitleModel getTitle();

    protected void onBindTitle(TitleModel title) {
        if (title == null || getViewState() == null) return;
        if (title.getTitleMessageRes() != 0) {
            getViewState().setTitleText(title.getTitleMessageRes());
        }

        if (!StringUtils.isNullOrEmpty(title.getTitleMessage())) {
            getViewState().setTitleText(title.getTitleMessage());
        }

        getViewState().setVisibilityBackButton(title.isVisibleBackButton());
        if (!StringUtils.isNullOrEmpty(title.getImageWeb())) {
            getViewState().setTitleImage(title.getImageWeb());
        }
    }

    public void updateTitle() {
        onBindTitle(getTitle());
    }

}
