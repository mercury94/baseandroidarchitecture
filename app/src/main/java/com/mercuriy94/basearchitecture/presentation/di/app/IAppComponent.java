package com.mercuriy94.basearchitecture.presentation.di.app;

import android.content.Context;

import com.mercuriy94.basearchitecture.presentation.di.datastore.DataStoreModule;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.PresenterBindingModule;
import com.mercuriy94.basearchitecture.presentation.di.rxschedulers.RxSchedulerModule;
import com.mercuriy94.basearchitecture.presentation.module.app.MyHomeApplication;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {
        AppModule.class,
        PresenterBindingModule.class,
        DataStoreModule.class,
        RxSchedulerModule.class})

public interface IAppComponent {

    Context context();

    MyHomeApplication inject(MyHomeApplication application);

}
