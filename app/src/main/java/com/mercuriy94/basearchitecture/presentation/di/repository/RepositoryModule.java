package com.mercuriy94.basearchitecture.presentation.di.repository;

import com.mercuriy94.basearchitecture.data.repository.currentuser.CurrentUserRepository;
import com.mercuriy94.basearchitecture.data.repository.currentuser.ICurrentUserRepository;

import dagger.Binds;
import dagger.Module;



/**
 * Created by Nikita on 13.04.2017.
 */

@Module(includes = {LocalRepositoryModule.class, WebRepositoryModule.class,})
public abstract class RepositoryModule {



    @Binds
    public abstract ICurrentUserRepository bindCurrentUserRepository(CurrentUserRepository currentUserRepository);



}
