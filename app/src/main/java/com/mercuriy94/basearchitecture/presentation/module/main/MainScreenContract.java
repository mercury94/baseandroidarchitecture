package com.mercuriy94.basearchitecture.presentation.module.main;

import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.presentation.common.presenter.BasePresenter;
import com.mercuriy94.basearchitecture.presentation.common.router.ActivityRouterAdapter;
import com.mercuriy94.basearchitecture.presentation.common.router.BaseActivityRouter;
import com.mercuriy94.basearchitecture.presentation.common.view.BaseActivity;
import com.mercuriy94.basearchitecture.presentation.common.view.IBaseView;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.basearchitecture.presentation.module.main.router.MainActivityRouterAdapter;


/**
 * Created by Nikita on 06.04.2017.
 */

public class MainScreenContract {

    public MainScreenContract() {
        throw new RuntimeException(getClass().getName() + " no instance please");
    }


    public interface IMainView extends IBaseView {

        void onStartDataSync();

        void onFinishDataSync();
    }

    public static abstract class AbstractMainView extends BaseActivity<AbstractMainPresenter, AbstractMainRouter>
            implements IMainView {

    }

    public static abstract class AbstractMainPresenter extends BasePresenter<IMainView,
                AbstractMainRouter> {

        public AbstractMainPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }
    }

    public static abstract class AbstractMainRouter extends BaseActivityRouter {

        public AbstractMainRouter(@NonNull ActivityRouterAdapter routerAdapter) {
            super(routerAdapter);
        }
    }

}
