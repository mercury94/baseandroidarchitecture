package com.mercuriy94.basearchitecture.presentation.model;

/**
 * Created by Nikita on 14.05.2017.
 */

public class MessageModel {

    private boolean mFromCurrentUser;
    private String mMessage;
    private boolean mSended;

    public boolean isFromCurrentUser() {
        return mFromCurrentUser;
    }

    public void setFromCurrentUser(boolean fromCurrentUser) {
        mFromCurrentUser = fromCurrentUser;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public boolean isSended() {
        return mSended;
    }

    public void setSended(boolean sended) {
        mSended = sended;
    }
}
