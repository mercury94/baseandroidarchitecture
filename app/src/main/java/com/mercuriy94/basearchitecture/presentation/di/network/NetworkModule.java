package com.mercuriy94.basearchitecture.presentation.di.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.mercuriy94.basearchitecture.data.repository.settings.connection.IConnectionSettingsRepository;
import com.mercuriy94.basearchitecture.presentation.di.app.AppModule;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nikita on 14.03.2017.
 */

@Module(includes = AppModule.class)
public class NetworkModule {


    public static final String TIME_FORMAT = "dd-MM-yyyy'T'HH:mm:ss";

    @NonNull
    @Provides
    protected Retrofit provideAuthenticationRetrofit(
            OkHttpClient okHttpClient,
            Retrofit.Builder baseRetrofitBuilder
    ) {
        return baseRetrofitBuilder
                .client(okHttpClient)
                .build();
    }

    @NonNull
    @Provides
    public Retrofit.Builder provideBaseRetrofitBuilder(
            IConnectionSettingsRepository connectionSettingsRepository,
            Gson gson
    ) {
        String address = connectionSettingsRepository.getIpAddress() +
                ":" +
                connectionSettingsRepository.getPort();
        return new Retrofit.Builder()
                .baseUrl(address)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @NonNull
    @Provides
    public OkHttpClient.Builder provideBaseOkHttpBuilder(IConnectionSettingsRepository connectionSettingsRepository) {
        final HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .connectTimeout(Long.parseLong(connectionSettingsRepository.getConnectTimeout()), TimeUnit.SECONDS)
                .writeTimeout(Long.parseLong(connectionSettingsRepository.getRequestTimeout()), TimeUnit.SECONDS)
                .readTimeout(Long.parseLong(connectionSettingsRepository.getResponseTimeout()), TimeUnit.SECONDS);
    }

    @NonNull
    @Provides
    public OkHttpClient provideAuthenticationOkHttpClient(OkHttpClient.Builder baseBuilder, Context context) {
        return baseBuilder.build();
    }

    @NonNull
    @Provides
    public Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(TIME_FORMAT)
                .create();
    }

    //region provides services and repos

}
