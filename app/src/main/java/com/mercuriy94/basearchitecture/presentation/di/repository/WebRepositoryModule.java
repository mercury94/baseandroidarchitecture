package com.mercuriy94.basearchitecture.presentation.di.repository;

import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.data.repository.currentuser.web.CurrentUserWebRepository;
import com.mercuriy94.basearchitecture.data.repository.currentuser.web.ICurrentUserWebRepository;
import com.mercuriy94.basearchitecture.presentation.di.network.NetworkModule;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import retrofit2.Retrofit;


/**
 * Created by nikita on 28.12.2016.
 */
@Module(includes = {NetworkModule.class, WebRepositoryModule.Declarations.class})
public class WebRepositoryModule {


    //endregion

    @Module
    public static abstract class Declarations {


        @NonNull
        @Binds
        public abstract ICurrentUserWebRepository bindCurrentUserWebRepository(CurrentUserWebRepository currentUserWebRepository);


    }

}
