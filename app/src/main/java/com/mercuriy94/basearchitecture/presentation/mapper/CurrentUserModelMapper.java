package com.mercuriy94.basearchitecture.presentation.mapper;

import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.data.entity.CurrentUser;
import com.mercuriy94.basearchitecture.data.mapper.Mapper;
import com.mercuriy94.basearchitecture.presentation.di.rxschedulers.RxSchedulerModule;
import com.mercuriy94.basearchitecture.presentation.model.CurrentUserModel;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;


/**
 * Created by Nikita on 08.05.2017.
 */

public class CurrentUserModelMapper extends Mapper<CurrentUser, CurrentUserModel> {


    @Inject
    public CurrentUserModelMapper(
            @Named(RxSchedulerModule.COMPUTATION) @NonNull Scheduler subscribeScheduler,
            @Named(RxSchedulerModule.MAIN) @NonNull Scheduler observerScheduler) {
        super(subscribeScheduler, observerScheduler);
    }

    @Override
    public CurrentUserModel transform(CurrentUser currentUser) {
        CurrentUserModel currentUserModel = new CurrentUserModel();
        currentUserModel.setName(currentUser.getName());
        currentUserModel.setSurname(currentUser.getSurname());
        return currentUserModel;
    }
}
