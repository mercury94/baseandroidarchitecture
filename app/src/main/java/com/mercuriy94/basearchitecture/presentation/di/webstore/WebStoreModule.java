package com.mercuriy94.basearchitecture.presentation.di.webstore;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.data.WebDataStore;
import com.mercuriy94.basearchitecture.presentation.di.repository.WebRepositoryModule;

import dagger.Binds;
import dagger.Module;


/**
 * Created by nikita on 25.12.2016.
 */

@Module(includes = WebStoreModule.Declarations.class)
public class WebStoreModule {

    @NonNull
    private final Context mContext;

    public WebStoreModule(@NonNull Context context) {
        mContext = context;
    }

    @Module(includes = WebRepositoryModule.class)
    public abstract class Declarations {

        @NonNull
        @Binds
        public abstract WebDataStore bindWebDataStore(WebDataStore webDataStore);

    }


}
