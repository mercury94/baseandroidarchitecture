package com.mercuriy94.basearchitecture.presentation.di.presenterbindings;

import com.mercuriy94.basearchitecture.presentation.common.presenter.BasePresenter;

import dagger.MapKey;

/**
 * Created by Nikita on 05.05.2017.
 */

@MapKey
public @interface PresenterKey {

    Class<? extends BasePresenter> value();

}
