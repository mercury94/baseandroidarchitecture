package com.mercuriy94.basearchitecture.presentation.module.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.mercuriy94.basearchitecture.presentation.common.presenter.BasePresenter;
import com.mercuriy94.basearchitecture.presentation.di.app.AppModule;
import com.mercuriy94.basearchitecture.presentation.di.app.DaggerIAppComponent;
import com.mercuriy94.basearchitecture.presentation.di.network.NetworkModule;
import com.mercuriy94.basearchitecture.presentation.di.objectbox.ObjectBoxModule;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.basearchitecture.presentation.di.presenterbindings.PresenterComponentBuilder;
import com.mercuriy94.basearchitecture.presentation.di.repository.LocalRepositoryModule;
import com.mercuriy94.basearchitecture.presentation.di.repository.WebRepositoryModule;
import com.mercuriy94.basearchitecture.presentation.di.rxschedulers.RxSchedulerModule;
import com.squareup.picasso.Picasso;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;


public class MyHomeApplication extends Application implements HasPresenterSubcomponentBuilders {

    public static final String TAG = "MyHomeApplication";

    @Inject
    Map<Class<? extends BasePresenter>, Provider<PresenterComponentBuilder>> mPresenterComponentBuilders;

    @Override
    public void onCreate() {
        super.onCreate();

        inject(this);

    }

    public static HasPresenterSubcomponentBuilders get(Context context) {
        return ((HasPresenterSubcomponentBuilders) context.getApplicationContext());
    }

    public void inject(Context context) {
        DaggerIAppComponent.builder()
                .appModule(new AppModule(context))
                .networkModule(new NetworkModule())
                .localRepositoryModule(new LocalRepositoryModule())
                .webRepositoryModule(new WebRepositoryModule())
                .objectBoxModule(new ObjectBoxModule())
                .rxSchedulerModule(new RxSchedulerModule())
                .build()
                .inject(this);
    }

    @Override
    public PresenterComponentBuilder getPresenterComponentBuilder(Class<? extends BasePresenter> presenterClass) {
        return mPresenterComponentBuilders.get(presenterClass).get();
    }
}
