package com.mercuriy94.basearchitecture.presentation.di.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by nikita on 23.03.2017.
 */
@Scope
@Retention(RUNTIME)
public @interface ServiceScope {
}
