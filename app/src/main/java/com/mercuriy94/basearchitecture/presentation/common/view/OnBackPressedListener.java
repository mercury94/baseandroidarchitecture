package com.mercuriy94.basearchitecture.presentation.common.view;

/**
 * Created by nikita on 15.03.2017.
 */

public interface OnBackPressedListener {

    boolean onBackPressed();
}
