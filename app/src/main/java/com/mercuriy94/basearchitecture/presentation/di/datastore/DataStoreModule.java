package com.mercuriy94.basearchitecture.presentation.di.datastore;

import com.mercuriy94.basearchitecture.presentation.di.repository.RepositoryModule;

import dagger.Binds;
import dagger.Module;


@Module(includes = RepositoryModule.class)
public  abstract class DataStoreModule {

}
