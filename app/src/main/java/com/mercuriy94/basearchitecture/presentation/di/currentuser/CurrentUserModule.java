package com.mercuriy94.basearchitecture.presentation.di.currentuser;

import com.mercuriy94.basearchitecture.data.repository.currentuser.local.CurrentUserLocalRepository;
import com.mercuriy94.basearchitecture.presentation.mapper.CurrentUserModelMapper;
import com.mercuriy94.basearchitecture.presentation.model.CurrentUserModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by Nikita on 12.04.2017.
 */

@Module
public class CurrentUserModule {

    @Singleton
    @Provides
    public CurrentUserModel providesCurrentUser(CurrentUserLocalRepository currentUserLocalRepository,
                                                CurrentUserModelMapper currentUserModelMapper) {

        return currentUserLocalRepository.getCurrentUser()
                .map(user -> user == null ? null : currentUserModelMapper.transform(user))
                .blockingSingle();
    }
}
