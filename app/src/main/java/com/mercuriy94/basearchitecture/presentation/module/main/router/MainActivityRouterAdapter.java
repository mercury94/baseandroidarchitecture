package com.mercuriy94.basearchitecture.presentation.module.main.router;

import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.presentation.common.router.ActivityRouterAdapter;
import com.mercuriy94.basearchitecture.presentation.common.view.BaseActivity;


/**
 * Created by Nikita on 15.04.2017.
 */

public class MainActivityRouterAdapter extends ActivityRouterAdapter {


    public MainActivityRouterAdapter(@NonNull BaseActivity baseActivity) {
        super(baseActivity);
    }

    public MainActivityRouterAdapter(@NonNull BaseActivity baseActivity, int containerForFragmentsId) {
        super(baseActivity, containerForFragmentsId);
    }
}
