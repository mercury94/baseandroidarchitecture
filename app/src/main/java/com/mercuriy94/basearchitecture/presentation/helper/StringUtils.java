package com.mercuriy94.basearchitecture.presentation.helper;

/**
 * Created by Nikita on 23.04.2017.
 */

public class StringUtils {

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }

}
