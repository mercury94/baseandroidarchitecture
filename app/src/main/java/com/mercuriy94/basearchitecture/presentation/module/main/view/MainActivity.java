package com.mercuriy94.basearchitecture.presentation.module.main.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mercuriy94.basearchitecture.presentation.R;
import com.mercuriy94.basearchitecture.presentation.common.annotations.Layout;
import com.mercuriy94.basearchitecture.presentation.common.router.ActivityRouterAdapter;
import com.mercuriy94.basearchitecture.presentation.helper.StringUtils;
import com.mercuriy94.basearchitecture.presentation.module.app.MyHomeApplication;
import com.mercuriy94.basearchitecture.presentation.module.main.MainScreenContract;
import com.mercuriy94.basearchitecture.presentation.module.main.presenter.MainPresenter;
import com.mercuriy94.basearchitecture.presentation.module.main.router.MainActivityRouterAdapter;
import com.mercuriy94.basearchitecture.presentation.module.main.router.MainRouter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;



@Layout(R.layout.activity_main)
public class MainActivity extends MainScreenContract.AbstractMainView {

    public MainActivity() {
    }

    @InjectPresenter
    MainScreenContract.AbstractMainPresenter mMainPresenter;



    @ProvidePresenter
    MainScreenContract.AbstractMainPresenter provideMainPresenter() {
        return new MainPresenter(MyHomeApplication.get(this));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void setInitialState() {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return mMainPresenter.onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    @Override
    public MainScreenContract.AbstractMainPresenter getPresenter() {
        return mMainPresenter;
    }

    @NonNull
    @Override
    public MainScreenContract.AbstractMainRouter createRouter(ActivityRouterAdapter activityRouteConductor) {
        return new MainRouter(new MainActivityRouterAdapter(this));
    }

    public MainActivity(MainScreenContract.AbstractMainPresenter mainPresenter) {
        mMainPresenter = mainPresenter;
    }

    @NonNull
    @Override
    protected ActivityRouterAdapter createRouterAdapter() {
        return new MainActivityRouterAdapter(this);
    }

    //region IBaseFragmentListener


    @Override
    public void setTitleText(@StringRes int titleText) {
    }

    @Override
    public void setTitleText(String title) {

    }

    @Override
    public void setTitleImage(String url) {
        if (!StringUtils.isNullOrEmpty(url)) {

        }
    }

    @Override
    public void setVisibilityBackButton(boolean value) {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(value);
    }

    @Override
    public void onStartDataSync() {

    }

    @Override
    public void onFinishDataSync() {

    }


    //endregion IBaseFragmentListener

}
