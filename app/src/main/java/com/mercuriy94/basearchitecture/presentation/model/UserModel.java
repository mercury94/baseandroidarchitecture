package com.mercuriy94.basearchitecture.presentation.model;

/**
 * Created by Nikita on 06.05.2017.
 */

public class UserModel {

    private String mName;
    private String mSurname;
    private String mAvatarUrl;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        mAvatarUrl = avatarUrl;
    }
}
