package com.mercuriy94.basearchitecture.data.repository.currentuser.web;

import com.mercuriy94.basearchitecture.data.entity.CurrentUser;
import com.mercuriy94.basearchitecture.data.entity.restservice.IUserService;

import javax.inject.Inject;

import io.reactivex.Observable;


/**
 * Created by Nikita on 29.04.2017.
 */

public class CurrentUserWebRepository implements ICurrentUserWebRepository {


    @Inject
    protected IUserService mUserService;

    @Inject
    public CurrentUserWebRepository() {
    }

    @Override
    public Observable<CurrentUser> getCurrentUser() {
        return null;
    }

}
