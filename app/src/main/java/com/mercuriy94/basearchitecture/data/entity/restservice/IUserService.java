package com.mercuriy94.basearchitecture.data.entity.restservice;

import com.mercuriy94.basearchitecture.data.entity.dto.retrofit.response.UserDto;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nikita on 02.01.2017.
 */

public interface IUserService {

    public static final String URL_USER_BASE = "user/";
    public static final String URL_USER_GET = URL_USER_BASE + "get/";
    public static final String URL_USER_GET_ALL = URL_USER_GET + "all/";
    public static final String URL_GET_CURRENT_USER = URL_USER_GET + "currentuser/";


    @GET(URL_GET_CURRENT_USER)
    Observable<UserDto> getCurrentUser();

    @GET(URL_USER_GET)
    Observable<UserDto> getUserByLogin(@Query("login") String login);

    @GET(URL_USER_GET_ALL)
    Observable<List<UserDto>> getAllUsers();




}
