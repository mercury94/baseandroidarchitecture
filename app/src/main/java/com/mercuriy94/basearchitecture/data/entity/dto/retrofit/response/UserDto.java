package com.mercuriy94.basearchitecture.data.entity.dto.retrofit.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikita on 02.01.2017.
 */

public class UserDto {

    public static final String FIELD_MAIL = "mail";
    public static final String FIELD_LOGIN = "login";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_SURNAME = "surname";
    public static final String FIELD_PATRONYMIC = "patronymic";
    public static final String FIELD_UUID = "uuid";
    public static final String FIELD_PHOTO_AVATAR = "photoAvatar";


    @SerializedName(FIELD_NAME)
    private String mName;
    @SerializedName(FIELD_SURNAME)
    private String mSurname;
    @SerializedName(FIELD_PATRONYMIC)
    private String mPatronymic;
    @SerializedName(FIELD_LOGIN)
    private String mLogin;
    @SerializedName(FIELD_MAIL)
    private String mMail;
    @SerializedName(FIELD_UUID)
    private String mUuid;
    @SerializedName(FIELD_PHOTO_AVATAR)
    private String mPhotoAvatar;


    public UserDto() {
    }


    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getMail() {
        return mMail;
    }

    public void setMail(String mail) {
        mMail = mail;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public String getPatronymic() {
        return mPatronymic;
    }

    public void setPatronymic(String patronymic) {
        mPatronymic = patronymic;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String uuid) {
        mUuid = uuid;
    }

    public String getPhotoAvatar() {
        return mPhotoAvatar;
    }

    public void setPhotoAvatar(String photoAvatar) {
        mPhotoAvatar = photoAvatar;
    }
}
