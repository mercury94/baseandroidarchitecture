package com.mercuriy94.basearchitecture.data.mapper;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.mercuriy94.basearchitecture.domain.common.DefaultObserver;

import java.util.List;

import dagger.internal.Preconditions;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;


/**
 * Created by Nikita on 06.05.2017.
 */

public abstract class Mapper<Source, Destination> {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    @NonNull
    protected Scheduler mSubscribeScheduler;
    @NonNull
    protected Scheduler mObserverScheduler;


    public Mapper(@NonNull Scheduler subscribeScheduler,
                  @NonNull Scheduler observerScheduler) {
        mSubscribeScheduler = subscribeScheduler;
        mObserverScheduler = observerScheduler;
    }

    private Mapper() {
    }

    public abstract Destination transform(Source source);

    public List<Destination> transform(List<Source> sourceList) {
        return Stream.of(sourceList).map(this::transform).toList();
    }

    public void rxTransform(DisposableObserver<Destination> disposable, Source source) {
        Preconditions.checkNotNull(disposable);
        final Observable<Destination> observable = Observable.fromCallable(() -> transform(source))
                .compose(applySchedulers());
        addDisposable(observable.subscribeWith(disposable));
    }

    public void rxTransform(DisposableObserver<List<Destination>> disposable, List<Source> sourceList) {
        Preconditions.checkNotNull(disposable);
        final Observable<List<Destination>> observable = Observable.fromCallable(() -> transform(sourceList))
                .compose(applySchedulers());
        addDisposable(observable.subscribeWith(disposable));
    }

    private <T> ObservableTransformer<T, T> applySchedulers() {
        return upstream -> upstream.subscribeOn(mSubscribeScheduler)
                .observeOn(mObserverScheduler);
    }

    public void dispose() {
        if (mCompositeDisposable != null && mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
    }

    protected void addDisposable(Disposable disposable) {
        Preconditions.checkNotNull(disposable);
        Preconditions.checkNotNull(mCompositeDisposable);
        mCompositeDisposable.add(disposable);
    }
}
