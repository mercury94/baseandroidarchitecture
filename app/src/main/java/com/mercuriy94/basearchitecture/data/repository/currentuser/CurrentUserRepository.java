package com.mercuriy94.basearchitecture.data.repository.currentuser;

import android.content.Context;

import com.mercuriy94.basearchitecture.data.entity.CurrentUser;
import com.mercuriy94.basearchitecture.data.repository.currentuser.local.ICurrentUserLocalRepository;

import javax.inject.Inject;

import io.reactivex.Observable;


/**
 * Created by Nikita on 29.04.2017.
 */

public class CurrentUserRepository implements ICurrentUserRepository {

    @Inject
    ICurrentUserLocalRepository mCurrentUserLocalRepository;


    @Inject
    Context mContext;

    @Inject
    public CurrentUserRepository() {
    }


    @Override
    public Observable<Long> saveCurrentUser(CurrentUser currentUser) {
        return null;
    }

    @Override
    public Observable<CurrentUser> getCurrentUser() {
        return null;
    }
}
