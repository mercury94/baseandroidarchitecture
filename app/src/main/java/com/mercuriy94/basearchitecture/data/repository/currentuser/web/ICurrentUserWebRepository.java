package com.mercuriy94.basearchitecture.data.repository.currentuser.web;

import com.mercuriy94.basearchitecture.data.entity.CurrentUser;

import io.reactivex.Observable;

/**
 * Created by Nikita on 29.04.2017.
 */

public interface ICurrentUserWebRepository {



    Observable<CurrentUser> getCurrentUser();

}
