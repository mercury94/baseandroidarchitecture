package com.mercuriy94.basearchitecture.data.repository.currentuser.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.mercuriy94.basearchitecture.data.LocalRepository;
import com.mercuriy94.basearchitecture.data.entity.CurrentUser;

import javax.inject.Inject;

import io.objectbox.Box;
import io.reactivex.Observable;

/**
 * Created by Nikita on 29.04.2017.
 */

public class CurrentUserLocalRepository extends LocalRepository<CurrentUser> implements ICurrentUserLocalRepository {

    @Inject
    public CurrentUserLocalRepository(@NonNull Context context,
                                      @NonNull Box<CurrentUser> box,
                                      @NonNull SharedPreferences sharedPreferences) {
        super(context, box, sharedPreferences);
    }

    @Override
    public Observable<Long> saveCurrentUser(CurrentUser currentUser) {
        return Observable.fromCallable(() -> mBox.put(currentUser));
    }

    @Override
    public Observable<CurrentUser> getCurrentUser() {
        return isExistCurrentUser()
                .compose(upstream -> upstream.blockingSingle() ?
                        Observable.fromCallable(() -> mBox.query().build().findFirst())
                        : Observable.just(NOT_FOUND_LOCAL_CURRENT_USER));
    }

    @Override
    public Observable<Boolean> isExistCurrentUser() {
        return Observable.fromCallable(() -> mBox.query().build().count() > 0);
    }
}
