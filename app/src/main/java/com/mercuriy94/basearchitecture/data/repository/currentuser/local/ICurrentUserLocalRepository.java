package com.mercuriy94.basearchitecture.data.repository.currentuser.local;

import com.mercuriy94.basearchitecture.data.entity.CurrentUser;

import io.reactivex.Observable;

/**
 * Created by Nikita on 29.04.2017.
 */

public interface ICurrentUserLocalRepository {

    CurrentUser NOT_FOUND_LOCAL_CURRENT_USER = new CurrentUser();

    Observable<Long> saveCurrentUser(CurrentUser currentUser);

    Observable<CurrentUser> getCurrentUser();

    Observable<Boolean> isExistCurrentUser();


}
